#!/usr/bin/env bash

# Ensure that current directory is the dotfiles repository.
FIRST_COMMIT_SHA='6ffb2354499359ddad11bd0b69667eeaf7640fdf'
if [ "$(git rev-list --max-parents=0 HEAD)" != "$FIRST_COMMIT_SHA" ]; then
  echo 'Not in dotfiles repository, exit.'
  exit 1
fi

cd home
for file_path in $(find . -type f); do
  source=$(realpath $file_path)
  dest=$(realpath $HOME/$(dirname $file_path))/$(basename $file_path)

  if [ -L $dest ]; then
    echo "Symlink $dest exists, replacing it..."
    rm $dest
    ln -s $source $dest

  elif [ -f $dest ]; then
    echo "Config file already exists: $dest"
    select res in "Diff" "Keep" "Replace"; do
      case $res in
        Diff )
          diff $source $dest;;
        Keep )
          echo "Ok. You can create a symlink later with 'ln -s $source $dest'."
          break;;
        Replace )
          echo "Moving old config file to $dest.old..."
          mv $dest $dest.old
          echo "Creating new symlink from $source to $dest..."
          ln -s $source $dest
          break;;
        *)
          echo "Invalid option $res";;
      esac
    done

  else
    if [ ! -d $(dirname $dest) ]; then
      echo "Directory $(dirname $dest) does not exist, creating it..."
      mkdir -p $(dirname $dest)
    fi 
    echo "Config file $dest does not exist, creating new symlink from $source..."
    ln -s $source $dest
  fi
done 

